public abstract class AbstractNode implements Comparable<AbstractNode>
{
	protected String key;

	public String key()
	{
		return key;
	}

	public abstract void display();

	public abstract Object clone();
}
