import java.util.Stack;

@SuppressWarnings("unchecked")
public class BST<T extends Node>
{
	private T root;
	private BST<T> parent;
	private BST<T> left;
	private BST<T> right;

	public BST()
	{
		root = null;
		parent = null;
		left = null;
		right = null;
	}

	public BST(T o)
	{
		root = o;
		parent = null;
		left = null;
		right = null;
	}

	public BST(BST<T> t)
	{
		root = t.root;
		parent = t.parent;
		left = t.left;
		right = t.right;
	}

	public void copy(BST<T> t)
	{
		root = (T)t.root.clone();
		parent = t.parent;
		left = t.left;
		right = t.right;
	}

	public void display()
	{
		if (left != null)
			left.display();

		root.display();

		if (right != null)
			right.display();
	}

	public void iterativeDisplay()
	{
		BST<T> n = this;
		Stack<BST<T>> parentStack = new Stack<BST<T>>();

		while (!parentStack.empty() || n != null) {
			if (n != null) {
				parentStack.push(n);
				n = n.left;
			}
			else {
				n = parentStack.pop();
				n.root.display();
				n = n.right;
			}
		}
	}

	public BST<T> search(String k)
	{
		BST<T> r = this;

		while (r != null && r.root.key().compareTo(k) != 0) {
			if (k.compareTo(r.root.key()) < 0)
				r = r.left;
			else
				r = r.right;
		}

		return r;
	}

	public void leafInsert(T o)
	{
		BST<T> y = new BST<T>(o);
		BST<T> x = this;
		BST<T> p = null;

		while (x != null && x.root != null) {
			p = x;
			int cmp =  y.root.compareTo(x.root);

			if (cmp == 0)
				return; // already in the tree
			else if (cmp < 0)
				x = x.left;
			else
				x = x.right;
		}

		y.parent = p;

		if (y.parent == null)
			copy(y);
		else if (y.root.compareTo(p.root) < 0)
			p.left = y;
		else
			p.right = y;

		balance(p);
	}

	public BST<T> maximum()
	{
		BST<T> r = this;

		if (r != null) {
			while (r.right != null)
				r = r.right;
		}

		return r;
	}

	public BST<T> predecessor()
	{
		return left.maximum();
	}

	public BST<T> minimum()
	{
		BST<T> r = this;

		if (r != null) {
			while (r.left != null)
				r = r.left;
		}

		return r;
	}

	public BST<T> successor()
	{
		return right.minimum();
	}

	private void move(BST<T> u, BST<T> v)
	{
		if (u.parent == null)
			copy(v);
		else if (u == u.parent.left)
			u.parent.left = v;
		else
			u.parent.right = v;

		if (v != null)
			v.parent = u.parent;
	}

	public void remove(String k)
	{
		BST<T> x = search(k);

		if (x.left == null) {
			move(x, x.right);
		}
		else if (x.right == null) {
			move(x, x.left);
		}
		else {
			BST<T> y = x.successor();

			if (y.parent != x) {
				move(y, y.right);
				y.right = x.right;
				y.right.parent = y;
			}

			if (x.left != null) {
				y.left = x.left;
				y.left.parent = y;
			}
			move(x, y);
		}
	}

	public int balanceFactor()
	{
		int leftHeight = 0;
		int rightHeight = 0;

		if (left != null)
			leftHeight = left.iterativeHeight() + 1;

		if (right != null)
			rightHeight = right.iterativeHeight() + 1;

		return (leftHeight - rightHeight);
	}

	public int height()
	{
		int leftHeight = 0;
		int rightHeight = 0;

		if (left != null)
			leftHeight = left.height() + 1;

		if (right != null)
			rightHeight = right.height() + 1;

		return Math.max(leftHeight, rightHeight);
	}

	public int iterativeHeight()
	{
		int height = 0;
		Stack<BST<T>> wq = new Stack<BST<T>>();
		Stack<BST<T>> path = new Stack<BST<T>>();
		BST<T> r = this;

		wq.push (r);
		while (!wq.empty()) {
			r = wq.peek();

			if (!path.empty() && r == path.peek()) {
				if (path.size() > height)
					height = path.size();

				path.pop();
				wq.pop();
			}
			else {
				path.push(r);

				if (r.right != null)
					wq.push(r.right);

				if (r.left != null)
					wq.push(r.left);
			}
		}

		return (height == 0) ? height : (height - 1);
	}

	public boolean contains(final BST<T> tree)
	{
		Stack<BST<T>> parentStack = new Stack<BST<T>>();
		BST<T> n = tree;

		while (!parentStack.empty() || n != null) {
			if (n != null) {
				if (search(n.root.key()) == null)
					return false;

				if (n.right != null)
					parentStack.push(n.right);

				n = n.left;
			}
			else {
				n = parentStack.pop();
			}
		}

		return true;
	}

	public boolean equivalent(final BST<T> tree)
	{
		return (contains(tree) && tree.contains(this));
	}

	public void fusion(final BST<T> tree)
	{
		Stack<BST<T>> parentStack = new Stack<BST<T>>();
		BST<T> n = tree;

		while (!parentStack.empty() || n != null) {
			if (n != null) {
				leafInsert(n.root);

				if (n.right != null)
					parentStack.push(n.right);

				n = n.left;
			}
			else {
				n = parentStack.pop();
			}
		}
	}

	public void leftRotation()
	{
		if (right != null) {
			BST<T> current = new BST<T>(this);
			if (current.left != null)
				current.left.parent = current;

			BST<T> R = right;
			current.right = R.left;
			if (current.right != null)
				current.right.parent = current;

			R.left = current;
			current.parent = this;
			R.parent = parent;
			if (R.right != null)
				R.right.parent = this;
			copy(R);
		}
	}

	public void rightRotation()
	{
		if (left != null) {
			BST<T> current = new BST<T>(this);
			if (current.right != null)
				current.right.parent = current;

			BST<T> L = left;
			current.left = L.right;
			if (current.left != null)
				current.left.parent = current;

			L.right = current;
			current.parent = this;
			L.parent = parent;
			if (L.left != null)
				L.left.parent = this;
			copy(L);
		}
	}

	public void leftRightRotation()
	{
		left.leftRotation();
		rightRotation();
	}

	public void rightLeftRotation()
	{
		right.rightRotation();
		leftRotation();
	}

	public void balance(BST<T> p)
	{
		while (p != null) {
			if (p.balanceFactor() == 2) {
				if (p.left != null && p.left.balanceFactor() == -1)
					p.leftRightRotation();
				else
					p.rightRotation();
			}
			else if (p.balanceFactor() == -2) {
				if (p.right != null && p.right.balanceFactor() == 1)
					p.rightLeftRotation();
				else
					p.leftRotation();
			}

			p = p.parent;
		}
	}
}
