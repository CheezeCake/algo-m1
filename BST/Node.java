public class Node extends AbstractNode
{
	public Node(String key)
	{
		this.key = new String(key);
	}

	public void display()
	{
		System.out.println(key);
	}

	public int compareTo(AbstractNode o)
	{
		return key.compareTo(((Node)o).key);
	}

	public Object clone()
	{
		return new Node(key);
	}
}
