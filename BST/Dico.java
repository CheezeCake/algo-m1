import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Dico
{
	public BST<Node> tree;

	public Dico()
	{
		tree = new BST<Node>();
	}

	public Dico(String file)
	{
		try {
			tree = new BST<Node>();

			BufferedReader br = new BufferedReader(new FileReader(file));
			String str;

			while ((str = br.readLine()) != null)
				tree.leafInsert(new Node(str));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void fusion(Dico d)
	{
		tree.fusion(d.tree);
	}
}
