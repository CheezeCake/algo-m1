import java.util.*;
import java.io.*;

public class TP
{
	public static void main(String[] args)
	{
		if (args.length != 2) {
			System.err.println("usage: java TP dictionaryFile textFile");
		}
		else {
			try {
				LinkedList<String> dictionary = new LinkedList<String>();
				BufferedReader br = new BufferedReader(new FileReader(args[0]));
				String line;
				// read dictionary
				while ((line = br.readLine()) != null)
					dictionary.add(line.toLowerCase());


				ChainingHashMap map1;
				LinearHashMap map2;
				DoubleHashMap map3;

				for (int i = 300; i <= 1000; i += 100) {
					map1 = new ChainingHashMap<Node>(i * 1000);

					insertDictionary(map1, dictionary);
					map1.checkSpelling(args[1]);
				}

				System.out.println();

				for (int i = 300; i <= 1000; i += 100) {
					map2 = new LinearHashMap<Node>(i * 1000);

					insertDictionary(map2, dictionary);
					map2.checkSpelling(args[1]);
				}

				System.out.println();

				for (int i = 300; i <= 1000; i += 100) {
					try {
						map3 = new DoubleHashMap<Node>(i * 1000);

						insertDictionary(map3, dictionary);
						map3.checkSpelling(args[1]);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void insertDictionary(AbstractHashMap map, LinkedList<String> dictionary)
	{
		for (String str : dictionary)
			map.put(str);
	}
}
