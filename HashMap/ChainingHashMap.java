import java.util.*;
import java.io.*;

@SuppressWarnings("unchecked")
public class ChainingHashMap<T extends Node> extends AbstractHashMap<T>
{
	private LinkedList[] table;

	public ChainingHashMap(int m)
	{
		this.m = m;
		table = new LinkedList[m];
	}

	public void put(String key)
	{
		int hash = h(key);
		Node value = new Node(key);

		if (table[hash] == null)
			table[hash] = new LinkedList<T>();

		table[hash].add(value);
	}

	protected int h(String key)
	{
		final int A = 1;
		int a = A;
		int x = 0;

		if (key.length() >= 0)
			x += key.charAt(0);

		for (int i = 1; i < key.length(); i++) {
			x += a * key.charAt(i);
			a *= A;
		}

		return (x % m);
	}

	public void display()
	{
		HashMap<Integer, Integer> nbElements = new HashMap<Integer, Integer>();

		for (int i = 0; i < m; i++) {
			int n = 0;
			if (table[i] != null)
				n = table[i].size();

			Integer value = nbElements.get(n);
			if (value == null)
				nbElements.put(n, 1);
			else
				nbElements.put(n, value + 1);
		}

		Set<Integer> keys = nbElements.keySet();
		for (Integer key : keys)
			System.out.println(key + " elements : " +
					nbElements.get(key) + " entries");
	}

	public T get(String key)
	{
		int hash = h(key);
		int i;

		if (table[hash] == null) {
			++wordsNotFound;

			return null;
		}

		for (i = 0; i < table[hash].size(); i++) {
			T node = (T)table[hash].get(i);

			if (node.key().equals(key)) {
				cmpFound += i + 1;
				++wordsFound;

				return node;
			}
		}

		cmpNotFound += i;
		++wordsNotFound;

		return null;
	}

	public String getType()
	{
		return "Chaining, m = " + m;
	}
}
