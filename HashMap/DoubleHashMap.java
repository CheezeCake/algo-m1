import java.util.*;
import java.io.*;

@SuppressWarnings("unchecked")
public class DoubleHashMap<T extends Node> extends AbstractHashMap<T>
{
	public enum NodeState { OCCUPIED, EMPTY, RELEASED }

	// private final int k = 1;
	private int k;
	private T[] table;
	private NodeState[] state;

	public DoubleHashMap(int m) throws Exception
	{
		this.m = m;
		table = (T[])new Node[m];

		state = new NodeState[m];
		for (int i = 0; i < m; i++)
			state[i] = NodeState.EMPTY;

		k = findCorpime(m);
	}

	private int E(int i, String key)
	{
		return Math.abs((h(key) + k * (i - 1)) % m);
	}

	public static int findCorpime(int m) throws Exception
	{
		for (int i = m - 1; i >= 1; i--) {
			if (gcd(i, m) == 1)
				return i;
		}

		throw new Exception("Couldn't find a number coprime with " + m);
	}

	public static int gcd(int a, int b)
	{
		int r;
		while (b != 0) {
			r = a % b;
			a = b;
			b = r;
		}

		return a;
	}

	public void put(String key)
	{
		if (full)
			return;

		int i = 1;
		boolean stop = false;
		int V = 0;

		while (i <= m && !stop) {
			V = E(i, key);

			if (state[V] == NodeState.OCCUPIED && !table[V].key().equals(key))
				++i;
			else
				stop = true;
		}

		if (stop) {
			if (state[V] != NodeState.OCCUPIED) {
				table[V] = (T)new Node(key);
				state[V] = NodeState.OCCUPIED;
			}
		}
		else {
			full = true;
		}
	}

	public T get(String key)
	{
		int i = 1;
		boolean stop = false;
		int V = 0;

		while (i <= m && !stop) {
			V = E(i, key);

			if (state[V] == NodeState.EMPTY ||
					state[V] == NodeState.OCCUPIED && table[V].key().equals(key))
				stop = true;
			else
				++i;
		}

		if (state[V] == NodeState.OCCUPIED && table[V].key().equals(key)) {
			++wordsFound;
			cmpFound += i;
			return table[V];
		}
		else {
			++wordsNotFound;
			cmpNotFound += i;

			if (!stop)
				--cmpNotFound;
		}

		return null;
	}

	public String getType()
	{
		return "Double, m = " + m;
	}
}
