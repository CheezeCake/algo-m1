import java.util.*;
import java.io.*;

@SuppressWarnings("unchecked")
public class LinearHashMap<T extends Node> extends AbstractHashMap<T>
{
	private T[] table;

	public LinearHashMap(int m)
	{
		this.m = m;
		table = (T[])new Node[m];
	}

	public void put(String key)
	{
		if (full)
			return;

		int hash = h(key);

		for (int i = 0; i < m; i++) {
			if (hash == m)
				hash = 0;

			if (table[hash] == null) {
				table[hash] = (T)new Node(key);
				return;
			}
			else if (table[hash].key().equals(key)) {
				return; // already in the table
			}

			++hash;
		}

		full = true;
	}

	public T get(String key)
	{
		int hash = h(key);
		T node;
		int i;

		for (i = 0; i < m; i++) {
			if (hash == m)
				hash = 0;

			if (table[hash] != null) {
				if (table[hash].key().equals(key)) {
					++wordsFound;
					cmpFound += i + 1;
					return table[hash];
				}
			}
			else {
				++wordsNotFound;
				cmpNotFound += i + 1;
				return null;
			}

			++hash;
		}

		++wordsNotFound;
		cmpNotFound += i;

		return null;
	}

	public String getType()
	{
		return "Linear, m = " + m;
	}
}
