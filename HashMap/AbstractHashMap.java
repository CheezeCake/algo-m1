import java.io.*;

public abstract class AbstractHashMap<T extends Node>
{
	protected int m;
	protected int cmpFound = 0;
	protected int wordsFound = 0;
	protected int cmpNotFound = 0;
	protected int wordsNotFound = 0;
	protected boolean full = false;

	protected int h(String key)
	{
		return (Math.abs(key.hashCode()) % m);
	}

	public abstract void put(String key);

	public abstract T get(String key);

	public abstract String getType();

	public void checkSpelling(String file)
	{
		long start = System.currentTimeMillis();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));

			int tmp;
			char c;
			String word = new String();

			while ((tmp = br.read()) != -1) {
				c = (char)tmp;

				if (c != ',' && c != ' ' && c != '.' && c != '(' && c != ')'
						&& c != '"' && c != '\n' && c != ':' && c != ';'
						&& c != '…' && c != '[' && c != ']' && c != '-'
						&& c != '?' && c != '!' && c != '\''
						&& (c < '0' || c > '9')) {

					word += c;
				}
				else if (!word.isEmpty()) {
					get(word.toLowerCase());
					word = "";
				}
			}

			System.out.printf( "%s \t"
					+ " %d words found, avg cmp: %.2f \t"
					+ " %d words not found, avg cmp: %.2f \t"
					+ " Total cmp: %d \t Time: %dms\n",
					getType(),
					wordsFound, (float)cmpFound / wordsFound,
					wordsNotFound, (float)cmpNotFound / wordsNotFound,
					cmpFound + cmpNotFound,
					System.currentTimeMillis() - start);
		}
		catch (IOException e) {
			System.out.println(e);
		}
		finally {
			cmpFound = 0;
			wordsFound = 0;
			cmpNotFound = 0;
			wordsNotFound = 0;
		}
	}
}
